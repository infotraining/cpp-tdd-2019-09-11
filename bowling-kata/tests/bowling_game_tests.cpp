#include "bowling_game.hpp"
#include "gmock/gmock.h"
#include "gtest/gtest.h"

using namespace std;

struct BowlingTests : ::testing::Test
{
    BowlingGame game;

    void roll_many(unsigned int count, unsigned int pins)
    {
        for (unsigned int i = 0; i < count; ++i)
            game.roll(pins);
    }

    void roll_spare()
    {
        game.roll(8);
        game.roll(2);
    }

    void roll_strike()
    {
        game.roll(BowlingGame::max_pins_in_frame);
    }
};

TEST_F(BowlingTests, WhenGameStartsScoreIsZero)
{
    ASSERT_EQ(game.score(), 0);
}

TEST_F(BowlingTests, WhenAllRollsInGuttersScoreIsZero)
{
    roll_many(20, 0);

    ASSERT_EQ(game.score(), 0);
}

TEST_F(BowlingTests, WhenNoMarksScoreIsSumOfPins)
{
    roll_many(20, 2);

    ASSERT_EQ(game.score(), 40);
}

TEST_F(BowlingTests, WhenSpareNextRollIsCountedTwice)
{
    roll_spare();
    roll_many(18, 1);

    ASSERT_EQ(game.score(), 29);
}

TEST_F(BowlingTests, WhenStrikeTwoNextRollsAreCountedTwice)
{
    roll_strike();
    roll_many(18, 1);

    ASSERT_EQ(game.score(), 30);
}

struct Bowling_MarksInLastFrame : BowlingTests
{
    void SetUp() override
    {
        roll_many(18, 1);
    }
};

TEST_F(Bowling_MarksInLastFrame, WhenSpareInLastFrameExtraRollIsAllowed)
{
    roll_spare(); // Act
    game.roll(5);

    ASSERT_EQ(game.score(), 33);
}

struct Bowling_PerfectGame : BowlingTests
{
};

TEST_F(Bowling_MarksInLastFrame, WhenStrikeInLastFrameTwoExtraRollsAreAllowed)
{
    roll_strike();
    game.roll(3);
    game.roll(3);

    ASSERT_EQ(game.score(), 34);
}

TEST_F(Bowling_PerfectGame, ScoreIs300)
{
    roll_many(12, BowlingGame::max_pins_in_frame);

    ASSERT_EQ(game.score(), 300);
}

struct  BowlingTestParams
{
    std::initializer_list<unsigned int> rolls;
    unsigned int expected_score;
};

std::ostream& operator<<(std::ostream& out, const BowlingTestParams& params)
{
    out << "{ rolls: [ ";
    for(const auto& pins : params.rolls)
        out << pins  << " ";

    out << "]; score = " << params.expected_score << " }";
    
    return out;
}

struct BowlingBulkTests : ::testing::TestWithParam<BowlingTestParams>
{};

TEST_P(BowlingBulkTests, GameScore)
{
    BowlingGame game;

    auto params = GetParam();

    for(const auto& pins : params.rolls)
    {
        game.roll(pins);
    }

    ASSERT_EQ(game.score(), params.expected_score);
}

BowlingTestParams bowling_test_input[] = {
    { {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}, 20},
    { {9, 1, 9, 1, 9, 1, 9, 1, 9, 1, 9, 1, 9, 1, 9, 1, 9, 1, 9, 1, 9}, 190},
    { {9, 0, 9, 0, 9, 0, 9, 0, 9, 0, 9, 0, 9, 0, 9, 0, 9, 0, 9, 0}, 90}
};

INSTANTIATE_TEST_CASE_P(PackOfBowlingScoreTests, BowlingBulkTests, 
    ::testing::ValuesIn(bowling_test_input));