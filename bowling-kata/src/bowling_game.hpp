#ifndef BOWLING_GAME_HPP
#define BOWLING_GAME_HPP

#include <algorithm>
#include <array>
#include <string>
#include <tuple>
#include <vector>

class BowlingGame
{
    std::array<unsigned int, 21> pins_ = {};
    unsigned int roll_no_ {};

    unsigned int frame_score(unsigned roll_index) const
    {
        return pins_[roll_index] + pins_[roll_index + 1];
    }

    bool is_spare(unsigned int roll_index) const
    {
        return frame_score(roll_index) == max_pins_in_frame;
    }

    unsigned int spare_bonus(unsigned int roll_index) const
    {
        return pins_[roll_index + 2];
    }

    bool is_strike(unsigned int roll_index) const
    {
        return pins_[roll_index] == max_pins_in_frame;
    }

    unsigned int strike_bonus(unsigned int roll_index) const
    {
        return pins_[roll_index + 1] + pins_[roll_index + 2];
    }

    auto score_for_frame(unsigned int roll_index) const
    {
        unsigned int result {};

        if (is_strike(roll_index))
        {
            result += max_pins_in_frame + strike_bonus(roll_index);
            ++roll_index;
        }
        else
        {
            if (is_spare(roll_index))
                result += spare_bonus(roll_index);

            result += frame_score(roll_index);

            roll_index += 2;
        }

        return std::tuple(result, roll_index);
    }

public:
    constexpr static unsigned int max_pins_in_frame = 10;
    constexpr static unsigned int frames_in_game = 10;

    unsigned int score() const
    {
        unsigned int result {};

        for (auto [frame, roll_index] = std::tuple(0u, 0u); frame < frames_in_game; ++frame)
        {
            auto [frame_score, next_roll_index] = score_for_frame(roll_index);

            result += frame_score;
            roll_index = next_roll_index;
        }

        return result;
    }

    void roll(unsigned int pins)
    {
        pins_[roll_no_] = pins;
        ++roll_no_;
    }
};

#endif