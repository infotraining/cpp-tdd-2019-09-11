#include <algorithm>

#include "data.hpp"
#include "gtest/gtest.h"
#include <gmock/gmock-matchers.h>

using namespace std;

TEST(TestGroup, FirstTest)
{
    vector<int> data = { 1, 6, 325, 235, 3, 534, 2367 };

    sort(data.begin(), data.end());

    ASSERT_TRUE(is_sorted(data.begin(), data.end()));
}