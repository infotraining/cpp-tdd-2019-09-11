#include "catch.hpp"
#include "source.hpp"

using namespace std;

TEST_CASE("pushing items to vector", "[vector][push]")
{
    vector<int> vec{3};

    REQUIRE(vec.size() == 1);

    SECTION("push_back")
    {
        vec.push_back(1);

        SECTION("size is increased")
        {
            REQUIRE(vec.size() == 2);
        }

        SECTION("push_back increses capacity")
        {
            REQUIRE(vec.capacity() >= 2);
        }
    }
}

SCENARIO("pushing items to vector")
{
    GIVEN("vector with data")
    {
        vector<int> vec = {1, 2, 3};
        auto prev_size = vec.size();

        WHEN("push_back() an item")
        {
            vec.push_back(4);

            THEN("size is increased")
            {
                REQUIRE(vec.size() - prev_size == 1);
            }
        }
    }
}