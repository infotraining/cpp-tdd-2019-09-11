# Test-Driven Development in C++

## Docs

* https://infotraining.bitbucket.io/cpp-tdd
* https://gitpitch.com/infotraining-dev/cpp-tdd-slides/master?grs=bitbucket#/12

## PROXY

### Proxy settings ###

* add to `.profile`

```
export http_proxy=http://10.144.1.10:8080
export https_proxy=http://10.144.1.10:8080
```

## Ankieta

* https://www.infotraining.pl/ankieta/cpp-tdd-2019-09-11-kp
